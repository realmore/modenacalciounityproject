using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System;
using ICSharpCode.SharpZipLib.Zip;

/// <summary>
/// A simple zip file helper class.
/// </summary>
public class ZipFile
{
    private List<string> Ext = new List<string>() { ".obj", ".jpg", ".png", ".mtl" };
    private enum Extension { model3d, jpg, png, material };
    FileInfo file = null;
    DirectoryInfo di = null;

    /// <summary>
    /// Write the given bytes data under the given filePath. 
    /// The filePath should be given with its path and filename. (e.g. c:/tmp/test.zip)
    /// </summary>
    public void UnZipObject(string filePath, byte[] data)
    {
        ZipConstants.DefaultCodePage = System.Text.Encoding.UTF8.CodePage;

        bool hasPassword = false;

        Debug.Log("Check password");
        if (IsPasswordProtected(data))
        {
            hasPassword = true;
        }
        Debug.Log("Enter zip file");
        using (ZipInputStream s = new ZipInputStream(new MemoryStream(data)))
        {
            if (hasPassword)
                s.Password = Constants.PASSWORD_ARCHIVE;

            ZipEntry theEntry;
            var newDirectoryPath = Directory.CreateDirectory(Path.Combine(filePath, "models"));
            Debug.Log("Create directory: " + newDirectoryPath.FullName);

            ZipConstants.DefaultCodePage = 0;

            while ((theEntry = s.GetNextEntry()) != null)
            {
                if (!Ext.Exists(x => x.ToLower() == Path.GetExtension(theEntry.Name).ToLower()) || theEntry.Name.Contains("__MACOSX/"))
                    continue;

                var entryFilePath = Path.Combine(newDirectoryPath.FullName, Path.GetFileName(theEntry.Name));

                using (FileStream streamWriter = File.Create(entryFilePath))
                {
                    if (Path.GetExtension(theEntry.Name).ToLower() == Ext[(int)Extension.model3d].ToString().ToLower())
                        file = new FileInfo(entryFilePath);

                    Debug.Log("File.Create: " + entryFilePath);
                    int size = 4096;
                    byte[] fdata = new byte[size];
                    while (true)
                    {
                        size = s.Read(fdata, 0, fdata.Length);
                        if (size > 0)
                        {
                            streamWriter.Write(fdata, 0, size);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
        }
        if (file == null)
        {
            Debug.Log("File .obj is null (there isn't?)");
        }
        else
        {
            di = new DirectoryInfo(Path.Combine(filePath, "models"));
        }
    }

    public GameObject CreateObject()
    {
        Debug.Log("Enter to creat object model");
        if (di != null && file != null)
        {
            var model = new OBJLoader().Load(Path.Combine(di.FullName, file.Name));
            Debug.Log("Created object: " + model);
            Array.ForEach(Directory.GetFiles(di.FullName), File.Delete);
            return model;
        }
        else
        {
            return null;
        }
    }

    private bool IsPasswordProtected(byte[] data)
    {
            using (ZipInputStream zip = new ZipInputStream(new MemoryStream(data)))
            {
                ZipEntry entry = zip.GetNextEntry();
                return entry.IsCrypted;
            }
    }
}
