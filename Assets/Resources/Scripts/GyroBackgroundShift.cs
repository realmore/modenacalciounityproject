﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Shifts selected RectTransform horizontally based on gyroscope instant acceleration
// Automatically determines bounds and avoids displaying over the edge, assuming the RectTransform is displaying full screen and centered
public class GyroBackgroundShift : MonoBehaviour
{

    Gyroscope gyro;
    private float boundMin;
    private float boundMax;
    private float interval;

    public RectTransform backgroundRT;
    public float displacementSpeed = 20000f;

    void Start()
    {
        gyro = Input.gyro;
        gyro.enabled = true;
        interval = gyro.updateInterval;
        if (interval == 0f) {
            interval = 0.02f; // for iPhone 7 and devices that return 0 for some reason, 50 hz should be fine
        }

        // Calculate left and right translation bound relative to starting position        
        boundMin = backgroundRT.rect.min.x + (backgroundRT.rect.width / (backgroundRT.anchorMax.x - backgroundRT.anchorMin.x) / 2f);
        boundMax = backgroundRT.rect.max.x - (backgroundRT.rect.width / (backgroundRT.anchorMax.x - backgroundRT.anchorMin.x) / 2f);
    }

    void Update()
    {

        backgroundRT.anchoredPosition += Vector2.right * gyro.rotationRateUnbiased.y * Time.deltaTime * interval * displacementSpeed;

        if (backgroundRT.anchoredPosition.x < boundMin) {
            backgroundRT.anchoredPosition = new Vector2(boundMin, backgroundRT.anchoredPosition.y);
        } else if (backgroundRT.anchoredPosition.x > boundMax) {
            backgroundRT.anchoredPosition = new Vector2(boundMax, backgroundRT.anchoredPosition.y);
        }

    }

}
