﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Threading.Tasks;

public class VideoTrackableEventHandler : DefaultTrackableEventHandler
{
    delegate void OnFinish();

    public ARVideoController videoController;
    public ARObjectController loadingController;
    public string vimeoClipURL;
    public float scale = 1f;
    public GameObject model3d;
    public JSONContentMetadata metadata;
    [SerializeField]
    private CloudHandler cloudHandler;
    [SerializeField]
    private ARUIHelper aRUIHelper;
    //Sound
    SoundManager soundManager;
    private bool targetIsLost = true;
    ZipFile zipFile = new ZipFile();

    #region PROTECTED_METHODS

    protected override void OnTrackingFound()
    {
        targetIsLost = false;
        if (!string.IsNullOrEmpty(vimeoClipURL))
        {
            StartVideoAR();
        }
        else if (metadata.Type == Constants.MODEL3D_TAG)
        {
            Start3DModelAR();
        }
        else if (metadata.Type == Constants.SANVALENTINO_TAG)
        {
            StartValentinsDayAR();
        }

    }

    private void StartVideoAR()
    {
        Debug.Log("---+ OnTrackingFound +--- " + vimeoClipURL);
        videoController.canvasGroup.alpha = 1f;
        videoController.transform.localPosition = Vector3.zero;
        videoController.transform.localRotation = Quaternion.Euler(0f, 180f, 0f);
        videoController.transform.localScale = new Vector3(0.1f * scale, 1f, 0.1f * scale);
        videoController.SetClip(vimeoClipURL);
        aRUIHelper.ToggleCrosshair(false);
        videoController.isTracking = true;
        if (videoController.canvas.renderMode == UnityEngine.RenderMode.ScreenSpaceOverlay)
        {
            videoController.ToggleFixedProjection();
        }
        if (videoController.canvas.renderMode == UnityEngine.RenderMode.WorldSpace)
        {
            videoController.Play();
        }
    }

    private void Start3DModelAR()
    {
        Debug.Log("---+ OnTrackingFound 3dModel+--- ");
        aRUIHelper.ToggleCrosshair(false);
        loadingController.canvasGroup.alpha = 1f;
        loadingController.transform.localPosition = Vector3.zero;
        loadingController.transform.localRotation = Quaternion.Euler(0f, 180f, 0f);
        loadingController.transform.localScale = new Vector3(0.1f * scale, 1f, 0.1f * scale);
        StartCoroutine(Download(metadata.Url, OnDownloadDone));
    }

    private void StartValentinsDayAR()
    {
        Debug.Log("---+ OnTrackingFound+--- ");
        aRUIHelper.ToggleCrosshair(false);
        aRUIHelper.Hearts(true);
        //Add sound track
        var heart = GameObject.Find("HeartsPS");
        soundManager = heart.AddComponent<SoundManager>();
        if (soundManager != null)
        {
            soundManager.soundTarget = (AudioSource)model3d.AddComponent<AudioSource>();
            soundManager.PlaySound(Constants.MODEL3D_SOUND_PATH);
        }
        if (!string.IsNullOrEmpty(metadata.WebsiteURI))
            aRUIHelper.ToggleWebsiteButtons(true);
    }

    private async Task UnzipObject(byte[] data)
    {
        //Because in thread we can't use Application.persistentDataPath
        string persistenPath = Application.persistentDataPath;
        await Task.Run(() => { zipFile.UnZipObject(persistenPath, data); });
    }

    private async void CreateObject(byte[] data)
    {
        await UnzipObject(data);
        model3d = zipFile.CreateObject();
        OnDownloadDone();
    }

    IEnumerator Download(string url, OnFinish onFinish)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else if (www.isDone)
        {
            byte[] data = www.downloadHandler.data;
            CreateObject(data);
        }
    }          

    void OnDownloadDone()
    {
        //Hide loading bar
        loadingController.canvasGroup.alpha = 0f;

        if (model3d != null)
        {
            if(targetIsLost == true)
            {
                Destroy(model3d);
                //Stop clip audio
                if (soundManager != null)
                {
                    soundManager.StopAllAudio();
                }
                cloudHandler.RestartCloudReco();
                return;
            }

            model3d.tag = Constants.MODEL3D_TAG;
            model3d.name = Constants.MODEL3D_TAG;
            model3d.transform.parent = mTrackableBehaviour.transform;
            model3d.transform.localScale = new Vector3(1f * scale, 1f * scale, 1f * scale);
            model3d.transform.localPosition = new Vector3(0f, 0f, 0f);
            model3d.transform.rotation = Quaternion.Euler(0f + metadata.RotationX, 90f + metadata.RotationY, 0f + metadata.RotationZ);

            model3d.AddComponent<TouchZoomInOut>();
            var collider = model3d.AddComponent<BoxCollider>();

            //Add sound track
            soundManager = model3d.AddComponent<SoundManager>();
            if (soundManager != null)
            {
                soundManager.soundTarget = (AudioSource)model3d.AddComponent<AudioSource>();
                soundManager.PlaySound(Constants.MODEL3D_SOUND_PATH);
            }

            //It is usefull for intercept touch on 3dObject
            collider.size = model3d.transform.localScale;

            if (!string.IsNullOrEmpty(metadata.WebsiteURI))
                aRUIHelper.ToggleWebsiteButtons(true);
                
            aRUIHelper.SliderZoom(true,model3d.transform.localScale);
        }
    }

    protected override void OnTrackingLost()
    {
        targetIsLost = true;
        if (vimeoClipURL != "" && videoController.isTracking)
        {
            videoController.isTracking = false;
            if (videoController.canvas.renderMode == UnityEngine.RenderMode.WorldSpace)
            {
                videoController.ToggleFixedProjection();
            }
        }
        else
        {
            aRUIHelper.ToggleCrosshair(true);
            if (loadingController != null)
            {
                loadingController.canvasGroup.alpha = 0f;
            }
        }
        if (model3d != null)
        {
            var foundTransforms = GameObject.FindGameObjectsWithTag(Constants.MODEL3D_TAG);
            if (foundTransforms != null)
            {
                foreach (var found in foundTransforms)
                    Destroy(found, 0);
            }
            cloudHandler.OnStateChanged(true);
            cloudHandler.RestartCloudReco();
            aRUIHelper.ToggleWebsiteButtons(false);
            aRUIHelper.SliderZoom(false, Vector3.one);

            //Stop clip audio
            if (soundManager != null)
            {
                soundManager.StopAllAudio();
            }

        }
        if(metadata != null && metadata.Type == Constants.SANVALENTINO_TAG)
        {
            aRUIHelper.Hearts(false);
            cloudHandler.OnStateChanged(true);
            cloudHandler.RestartCloudReco();
            aRUIHelper.ToggleWebsiteButtons(false);
            //Stop clip audio
            if (soundManager != null)
            {
                soundManager.StopAllAudio();
            }
        }
    }

    protected IEnumerator RestartRecoAfterSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (!videoController.isTracking)
        {
            videoController.UnloadVideo(); // Must unload video from video player to allow another to load
            cloudHandler.RestartCloudReco();
        }
    }

    #endregion // PROTECTED_METHODS

    public void RestartReco()
    {
        targetIsLost = true;
        videoController.isTracking = false;
        videoController.Stop();
        vimeoClipURL = "";
        if (videoController.canvas.renderMode == UnityEngine.RenderMode.ScreenSpaceOverlay)
        {
            videoController.ToggleFixedProjection();
        }
        StartCoroutine(RestartRecoAfterSeconds(0f));
    }

}
