﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoomManager : MonoBehaviour
{
    public Slider slider;

    public void SetSize()
    {
        var obj = GameObject.FindWithTag(Constants.MODEL3D_TAG);
        if (obj != null && slider != null)
        {
            obj.transform.localScale = Vector3.one * slider.value;
        }
    }

    public void UpdateMinValue(Vector3 minValue)
    {
        if(slider != null)
        {
            slider.minValue = minValue.x;
            slider.value = slider.minValue;
        }
    }
}
