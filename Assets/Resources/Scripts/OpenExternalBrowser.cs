﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenExternalBrowser : MonoBehaviour
{
    public string pageToOpen = "https://www.google.it/";

    public void OpenPage()
    {
        Application.OpenURL(pageToOpen);
    }
}
