﻿using UnityEngine;
using System.Collections;

public class BrowserManager : MonoBehaviour {

	public bool isBrowserOpen = false;
	public CloudHandler cloudHandler;
	public UniWebView uvw;
	public ARUIHelper aRUIHelper;
	public RectTransform webRectTrans;
	public CanvasGroup cg;
	public string domain = "";
	public string pageToOpen = "http://www.google.com";
	public string searchResultsPage = "app-search-results";
	public DeviceOrientation deviceOrientationManager;

	void Start () {
		uvw.SetBackButtonEnabled(false);
		uvw.ReferenceRectTransform = webRectTrans;
		Input.location.Start();

		// Intercept links with "fb://" schema and open them with a system call
		uvw.AddUrlScheme("fb");
		uvw.OnMessageReceived += (view, message) => {
			Application.OpenURL(message.RawMessage);
    	};
	}

	public void OpenInAppBrowser() {
		isBrowserOpen = true;
		cg.alpha = 1f;
		cg.blocksRaycasts = true;
		if (cloudHandler) {
			cloudHandler.StopCloudReco();
		}
		if (deviceOrientationManager) {
			deviceOrientationManager.SetScreenOrientation(ScreenOrientation.Portrait);
		}
        if (aRUIHelper)
        {
            aRUIHelper.VerticalButtons(false);
        }
		StartCoroutine(UpdateAndShow());
		StartCoroutine(AccessLocationAndLoadPage());
	}

	IEnumerator UpdateAndShow() {
		yield return new WaitForSeconds(1f);
		uvw.UpdateFrame();
		uvw.Show(true, UniWebViewTransitionEdge.Bottom, 0.35f);
		yield return new WaitForSeconds(0.5f);
		cg.interactable = true;
	}

	IEnumerator AccessLocationAndLoadPage() {

        if (!Input.location.isEnabledByUser) {
			uvw.Load(domain + pageToOpen);
			yield break;
		}

        Input.location.Start();
		int maxWait = 10;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
            yield return new WaitForSeconds(0.2f);
        	Input.location.Stop();
            maxWait--;
        }
        if (maxWait < 1) {
			uvw.Load(domain + pageToOpen);
        	Input.location.Stop();
            yield break;
        }
        if (Input.location.status == LocationServiceStatus.Failed) {
			uvw.Load(domain + pageToOpen);
        	Input.location.Stop();
            yield break;
        } else {
			string geolocationQuery = "?radius_key=" + Input.location.lastData.latitude + "," + Input.location.lastData.longitude;
			uvw.Load(domain + pageToOpen + geolocationQuery);
        	Input.location.Stop();
        }

	}

    public void RefreshCache()
    {
        if (uvw != null)
            uvw.CleanCache();
    }


    public void GoBack()
    {
        if (uvw == null)
            return;
        if (uvw.CanGoBack)
            uvw.GoBack();
    }

    public void GoForward()
    {
        if (uvw == null)
            return;
        if (uvw.CanGoForward)
            uvw.GoForward();
    }

    public void CloseInAppBrowser() {
		isBrowserOpen = false;
		cg.alpha = 0f;
		cg.interactable = false;
		cg.blocksRaycasts = false;
		if (cloudHandler != null) {
			cloudHandler.RestartCloudReco();
		}
		if (deviceOrientationManager != null) {
			deviceOrientationManager.SetScreenOrientation(deviceOrientationManager.orientationAtSceneStart);
		}
		uvw.Hide(true, UniWebViewTransitionEdge.Bottom, 0.35f);
		if (aRUIHelper) {
			aRUIHelper.ToggleCrosshair(true);
            aRUIHelper.VerticalButtons(true);
		}
	}

	public void BackToMobileHomepage() {
		pageToOpen = searchResultsPage;
		StartCoroutine(AccessLocationAndLoadPage());
	}

	public void Share() {
		uvw.EvaluateJavaScript("window.location.href", (payload) => {
			NativeShare ns = new NativeShare();
			ns.SetSubject("Guarda cosa ho trovato su Vizual");
			ns.SetText(payload.data.ToString());
			ns.SetTitle("Condivisione contenuto");
			ns.Share();
		});
	}

}
