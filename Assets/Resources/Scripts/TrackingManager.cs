﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingManager : MonoBehaviour {

	public CloudHandler cloudHandler;
	public ARVideoController videoController;

	public void StopAndRestartTracking () {
		StartCoroutine(StopAndRestartWithWait());
	}

	// Stops tracking all objects, waits for 2 seconds and then starts looking for trackables again
	IEnumerator StopAndRestartWithWait () {
		Vuforia.TrackerManager.Instance.GetTracker<Vuforia.ObjectTracker>().Stop();
		videoController.Stop();
		videoController.UnloadVideo();
		yield return new WaitForSeconds(2f);
		Vuforia.TrackerManager.Instance.GetTracker<Vuforia.ObjectTracker>().Start();
		cloudHandler.RestartCloudReco();
	}

}
