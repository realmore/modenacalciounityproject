﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource soundTarget;
    public AudioClip clipTarget;
    private AudioSource[] allAudioSource;

    //Function to stop all sounds
    public void StopAllAudio()
    {
        allAudioSource = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        if (allAudioSource == null)
            return;
        foreach (AudioSource audioS in allAudioSource)
        {
            audioS.Stop();
        }
    }

    //Function to play sound
    public void PlaySound(string ss)
    {
        clipTarget = (AudioClip)Resources.Load(ss);
        soundTarget.clip = clipTarget;
        soundTarget.loop = true;
        soundTarget.playOnAwake = false;
        soundTarget.volume = 0.5f;
        soundTarget.Play();
    }
}
