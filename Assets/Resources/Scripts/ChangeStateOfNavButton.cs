﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeStateOfNavButton : MonoBehaviour
{
    public Button btnBack;
    public Button btnForward;

    public GameObject browser;
    BrowserManager browserManager;

    // Start is called before the first frame update
    void Start()
    {
        if (browser != null)
        {
            browserManager = browser.GetComponent<BrowserManager>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(browserManager != null)
        {
            if (btnBack != null)
            {
                CheckStateBrowser(btnBack);
            }
            if (btnForward != null)
            {
                CheckStateBrowser(btnForward);
            }
        }
    }

    void CheckStateBrowser(Button btn)
    {
        if (btn != null)
        {
            if (browser != null)
            {
                var state = btn.name == "Back" ? browserManager.uvw.CanGoBack : browserManager.uvw.CanGoForward;
                if (state)
                {
                    btn.interactable = true;
                }
                else
                {
                    btn.interactable = false;
                }
            }
        }
    }
}
