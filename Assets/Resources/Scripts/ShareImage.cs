﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShareImage : MonoBehaviour
{
    public string Url;

    public void Share()
    {
        if(!string.IsNullOrEmpty(Url))
            new NativeShare().AddFile(Url).Share();
    }
}
