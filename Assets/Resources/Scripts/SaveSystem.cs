﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem {
    
    public static void SaveUserProfile(User user) {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/user.profile";
        FileStream stream = new FileStream(path, FileMode.Create);

        UserProfile data = new UserProfile(user);
        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static UserProfile LoadPlayer() {
        string path = Application.persistentDataPath + "/user.profile";

        if (File.Exists(path)) {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            UserProfile data = formatter.Deserialize(stream) as UserProfile;
            stream.Close();
            return data;
        } else {
            Debug.Log("FILE DOESNT EXIST");
            return null;
        }
    }
}
