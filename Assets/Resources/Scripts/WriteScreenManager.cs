﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using DigitsNFCToolkit.Samples;
using DigitsNFCToolkit;

public class WriteScreenManager : MonoBehaviour
{

    private NDEFMessage pendingMessage;
    [SerializeField]
    private MessageScreenView messageScreenView;

    [SerializeField]
    public User profile;
    [SerializeField]
    public UniWebView webView;
    [SerializeField]
    public AnimationGif readingStatus;
    [SerializeField]
    public Browsing browsing;

    private void Start()
    {
#if (!UNITY_EDITOR)
				NativeNFCManager.AddNDEFWriteFinishedListener(OnNDEFWriteFinished);
				NativeNFCManager.AddNDEFPushFinishedListener(OnNDEFPushFinished);
#endif
        profile.LoadUser();
    }

	#region "WRITING"
    public void OnAddRecordClick()
    {
        readingStatus.reading = true;
        readingStatus.error = false;
        if (pendingMessage == null)
        {
            pendingMessage = new NDEFMessage();
        }

        NDEFRecord record = null;
        NDEFRecordType type = NDEFRecordType.URI;
        //string uri = webView.Url;
        string uri = "https://myvizual.com/" + browsing.getUsername();
        Debug.Log("ProfileUrl: " + uri);
        record = new UriRecord(uri);

        pendingMessage.Records.Add(record);

        OnWriteMessageClick();
    }

    public void OnWriteMessageClick()
    {
        if (pendingMessage != null)
        {

#if (!UNITY_EDITOR)
				NativeNFCManager.RequestNDEFWrite(pendingMessage);
#if UNITY_ANDROID
				webView.Hide(true, UniWebViewTransitionEdge.Bottom, 0.35f);
				messageScreenView.Show();
				messageScreenView.SwitchToPendingWrite();
#elif UNITY_IOS
                NativeNFCManager.Enable(); //Show native popup
#endif
#endif
        }
    }

    public void OnWriteOKClick()
    {

        messageScreenView.Hide();
        webView.Show(true, UniWebViewTransitionEdge.Bottom, 0.35f);
        pendingMessage = null;
        readingStatus.reading = false;
        readingStatus.error = false;
        //view.CleanupRecordItems();
    }

    public void OnWriteCancelClick()
    {

        messageScreenView.Hide();
        webView.Show(true, UniWebViewTransitionEdge.Bottom, 0.35f);
#if (!UNITY_EDITOR)
			NativeNFCManager.CancelNDEFWriteRequest();
#endif
        readingStatus.error = false;
        readingStatus.reading = false;
    }


    public void OnNDEFWriteFinished(NDEFWriteResult result)
    {
        //view.UpdateNDEFMessage(result.Message);
        readingStatus.reading = false;
        string writeResultString = string.Empty;
        if (result.Success)
        {
            writeResultString = string.Format("Tag associato!", result.TagID);
            pendingMessage = null;
            //view.CleanupRecordItems();
        }
        else
        {
            readingStatus.error = true;
            writeResultString = string.Format("Errore nell'associazione con il tag.\nRiprovare", result.TagID, result.Error);
        }
        Debug.Log(writeResultString);
        messageScreenView.SwitchToWriteResult(writeResultString);
    }

    public void OnNDEFPushFinished(NDEFPushResult result)
    {
        // view.UpdateNDEFMessage(result.Message);

        string pushResultString = string.Empty;
        if (result.Success)
        {
            pushResultString = "NDEF Message pushed successfully to other device";
        }
        else
        {
            pushResultString = "NDEF Message failed to push to other device";
        }
        Debug.Log(pushResultString);
        messageScreenView.SwitchToPushResult(pushResultString);
    }
	#endregion
}