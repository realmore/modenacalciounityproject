﻿
[System.Serializable]
public class UserProfile {

    public string[] field = new string[15];
    public int selected_index;
    public string selected_social;


    public UserProfile(User user) {

        for (int i = 0; i < field.Length; i++) {
            field[i] = user.field[i];
        }
        selected_index = user.selected_index;
        selected_social = user.selected_social;
    }
}