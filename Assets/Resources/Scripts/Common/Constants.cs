﻿using UnityEngine;

public static class Constants
{
    public static string MODEL3D_TAG = "3D";

    public static string MODEL3D_SOUND_PATH = "Sounds/3dSound";

    public static string SANVALENTINO_TAG = "SanValentino";

    public static string PASSWORD_ARCHIVE = "V!ZuaL_@2019-Admin";

    public static Vector3 PRODUCT_SCALE = new Vector3(0.1f, 0.1f, 0.1f);
}
