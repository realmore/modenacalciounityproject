﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ARUIHelper : MonoBehaviour {

    [SerializeField]
    private ARVideoController arvc;
    [SerializeField]
    private BrowserManager browserManager;

    //Is used for make photo
    [SerializeField]
    private GameObject[] ElementsScene;

    [SerializeField]
    private GameObject[] interactionGOs;
    [SerializeField]
    private Button[] openWebsiteButtons;
    [SerializeField]
    private Button[] disableAutolaunchButtons;
    [SerializeField]
    private Image[] crosshairs;
    [SerializeField]
    private Text[] countdowns;

    [SerializeField]
    private GameObject[] Logo;

    [SerializeField]
    private GameObject[] verticalsGUI;

    [SerializeField]
    private GameObject[] SlidersZoom;

    [SerializeField]
    private GameObject[] loadingBars;

    [SerializeField]
    private GameObject[] Items3dGroundPlane;

    [SerializeField]
    private GameObject[] PanelsInstruction;

    [SerializeField]
    private GameObject[] NotifiesSavedPhoto;

    [SerializeField]
    private GameObject[] BlurEffect;

    [SerializeField]
    private GameObject[] HeartsPS;

    [SerializeField]
    private GameObject[] PlaneFinders;

    public bool loadingNewScene = false;

    public void OpenMainScene () {
        StartCoroutine(OpenSceneAsync("Main"));
    }

    IEnumerator OpenSceneAsync (string scene) {
        loadingNewScene = true;
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene);
        while (!asyncLoad.isDone) {
            yield return null;
        }
    }

    public void OpenWebsite () {
        arvc.Stop();
        arvc.canvasGroup.alpha = 0f;
        browserManager.OpenInAppBrowser();
    }

    public void ToggleAutolaunchButtons (bool active) {
        foreach (Button disableAutolaunchButton in disableAutolaunchButtons) {
            disableAutolaunchButton.gameObject.SetActive(active);
        }
    }

    public void ToggleWebsiteButtons (bool active) {
        foreach (GameObject interactionGO in interactionGOs) {
            interactionGO.SetActive(active);
        }
    }

    public void PanelInstruction(bool active)
    {
        foreach (GameObject panel in PanelsInstruction)
        {
            panel.SetActive(!panel.activeSelf);
        }
    }

    public void NotifySavedPhoto(bool active)
    {
        foreach (GameObject notify in NotifiesSavedPhoto)
        {
            notify.SetActive(active);
        }
    }

    public void VerticalButtons(bool active)
    {
        foreach (GameObject vertical in verticalsGUI)
        {
            vertical.SetActive(active);
        }
    }

    public void SetLogoVisibility(bool active)
    {
        foreach (GameObject image in Logo)
        {
            image.SetActive(active);
        }
    }

    public void SetElementsUIVisibility(bool active)
    {
        if (ElementsScene != null && ElementsScene.Any())
        {
            foreach (GameObject item in ElementsScene)
            {
                if (item.name == "share" && item.active == false && active)
                    continue;
                item.SetActive(active);
            }
        }
    }

    public void BlurEffects(bool active)
    {
        foreach (GameObject panel in BlurEffect)
        {
            panel.SetActive(active);
        }
    }

    public void SliderZoom(bool active, Vector3 minValue)
    {
        foreach (GameObject slider in SlidersZoom)
        {
            slider.SetActive(active);
            if (active != false)
            {
                if (minValue != Vector3.one)
                {
                    var component = slider.GetComponent<ZoomManager>();
                    if (component != null)
                    {
                        component.UpdateMinValue(minValue);
                    }
                }
            }
        }
    }

    public void ToggleCrosshair (bool active) {
        foreach (Image crosshair in crosshairs) {
            crosshair.gameObject.SetActive(active);
        }
    }

    public void Hearts(bool active)
    {
        foreach (GameObject heart in HeartsPS)
        {
            heart.SetActive(active);
        }
    }

    public void LoadingBar(bool active)
    {
        foreach (GameObject loadingBar in loadingBars)
        {
            loadingBar.SetActive(active);
        }
    }


    public void ProcessRemainingVideoTime (int seconds) {
        if (seconds < 6 && seconds > 0) {
            foreach (Text countdown in countdowns) {
                countdown.text = "Apertura sito tra " + seconds.ToString();
            }
        } else {
            foreach (Text countdown in countdowns) {
                countdown.text = "";
            }
        }
    }

}
