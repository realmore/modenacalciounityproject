﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraFlashHandler : MonoBehaviour {

    public Image[] cameraFlashes;
    public Color offColor;
    public Color onColor;

    private bool isFlashActive = false;

    private IEnumerator Start() {
        // Certain Samsung devices (e.g. S7), are not handled correctly by Vuforia 8's SetFlashTorchMode
        // Only show FlashTorch button if this feature can be handled by Vuforia
        // Handling the Flash Torch natively will break Vuforia functionality, even when stopping and restarting
        yield return new WaitForSeconds(2);
        if (Vuforia.CameraDevice.Instance.SetFlashTorchMode(false)) {
            foreach (Image cameraFlash in cameraFlashes) {
                if(cameraFlash != null)
                    cameraFlash.gameObject.SetActive(true);
            }
        }
    }

    public void SetCameraFlash (bool active) {
        Vuforia.CameraDevice.Instance.SetFlashTorchMode(active);
        isFlashActive = active;
        foreach (Image cameraFlash in cameraFlashes) {
            cameraFlash.color = active ? onColor : offColor;
        }
    }

    public void ToggleCameraFlash () {
        SetCameraFlash(!isFlashActive);
    }

}
