﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Networking;
using Vuforia;

[System.Serializable]
public class JSONContentMetadata {
	public string Title;
	public string WebsiteURI;
    public string Url;
    public string Mtl;
    public string[] Websites;
    public string Type;
	public string Value;
	public float Scale;
    public float RotationX;
    public float RotationY;
    public float RotationZ;
    public bool IsExternalWeb;
}

public class CloudHandler : MonoBehaviour, IObjectRecoEventHandler {

    public CloudRecoBehaviour mCloudRecoBehaviour;
	private bool mIsScanning = false;
	private string mTargetMetadata = "";

    public ImageTargetBehaviour ImageTargetTemplate;
	public VideoTrackableEventHandler videoTrackableEventHandler;
	public BrowserManager browserManager;

	void Start () {
        // register this event handler at the cloud reco behaviour
        mCloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();
	
		if (mCloudRecoBehaviour) {
			mCloudRecoBehaviour.RegisterEventHandler(this);
		}

	}

	public void OnInitialized(TargetFinder targetFinder) {
		Debug.Log ("Cloud Reco initialized");
	}

	public void OnInitError(TargetFinder.InitState initError) {
		Debug.Log ("Cloud Reco init error " + initError.ToString());
	}

	public void OnUpdateError(TargetFinder.UpdateState updateError) {
        var foundTransforms = GameObject.FindGameObjectsWithTag(Constants.MODEL3D_TAG);
        if (foundTransforms != null)
        {
            foreach (var found in foundTransforms)
                Destroy(found, 0);
        }
        Debug.Log ("Cloud Reco update error " + updateError.ToString());
	}

	public void OnStateChanged(bool scanning) {
		Debug.Log("CloudHandler - OnStateChanged - scanning: " + scanning);
		mIsScanning = scanning;
		if (scanning) {
			// clear all known trackables
			var tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
			tracker.GetTargetFinder<ImageTargetFinder>().ClearTrackables(false);
		}
	}

	// Here we handle a cloud target recognition event
	public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult) {
		// Do stuff with the target metadata
		mTargetMetadata = ((TargetFinder.CloudRecoSearchResult) targetSearchResult).MetaData;
		Debug.Log(mTargetMetadata);
		JSONContentMetadata metaData = JsonUtility.FromJson<JSONContentMetadata>(mTargetMetadata);

        browserManager.RefreshCache();

        if (metaData.IsExternalWeb == true)
        {
            browserManager.domain = metaData.WebsiteURI;
            browserManager.pageToOpen = "";
        }
        else
        {
            browserManager.domain = "https://www.vizual.it/";
            browserManager.pageToOpen = metaData.WebsiteURI;
        }
        if (metaData.Type == "Vimeo" && metaData.Value != null) {
            videoTrackableEventHandler.vimeoClipURL = metaData.Value;
			if (metaData.Scale > 0f) {
				videoTrackableEventHandler.scale = metaData.Scale;
			} else {
				videoTrackableEventHandler.scale = 1f;
			}
		}
        else if(metaData.Type == "Zodiaco")
        {
            int indexDay = (int)System.DateTime.Now.DayOfWeek == 0 ? 6 : (int)System.DateTime.Now.DayOfWeek - 1;
            browserManager.pageToOpen = metaData.Websites[indexDay];
            if (metaData.Value != null)
            {
                videoTrackableEventHandler.vimeoClipURL = metaData.Value;
                if (metaData.Scale > 0f)
                {
                    videoTrackableEventHandler.scale = metaData.Scale;
                }
                else
                {
                    videoTrackableEventHandler.scale = 1f;
                }
            }
            else
            {
                browserManager.OpenInAppBrowser();
            }
        }
        else if (metaData.Type == Constants.MODEL3D_TAG && metaData.Url != null)
        {
            videoTrackableEventHandler.metadata = metaData;
            browserManager.pageToOpen = metaData.WebsiteURI;
            if (metaData.Scale > 0f)
            {
                videoTrackableEventHandler.scale = metaData.Scale;
            }
            else
            {
                videoTrackableEventHandler.scale = 1f;
            }
        }
        else if(metaData.Type == Constants.SANVALENTINO_TAG)
        {
            videoTrackableEventHandler.metadata = metaData;
            browserManager.pageToOpen = metaData.WebsiteURI;
            if (metaData.Scale > 0f)
            {
                videoTrackableEventHandler.scale = metaData.Scale;
            }
            else
            {
                videoTrackableEventHandler.scale = 1f;
            }
        }


        // stop the target finder (i.e. stop scanning the cloud)
        mCloudRecoBehaviour.CloudRecoEnabled = false;

        // Build augmentation based on target
        if (ImageTargetTemplate) {
			// enable the new result with the same ImageTargetBehaviour:
			ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
			ImageTargetBehaviour imageTargetBehaviour = (ImageTargetBehaviour)tracker.GetTargetFinder<ImageTargetFinder>().EnableTracking(targetSearchResult, ImageTargetTemplate.gameObject);
		}
	}


    public void StopCloudReco() {
		mCloudRecoBehaviour.CloudRecoEnabled = false; // Stop cloud reco
		TrackerManager.Instance.GetTracker<ObjectTracker>().Stop(); // Also stop tracking already reco'd target
		Debug.Log("Cloud reco stopped");
	}

    // Start looking again for new markers to recognize with cloud
    public void RestartCloudReco() {
		videoTrackableEventHandler.videoController.UnloadVideo();
		mCloudRecoBehaviour.CloudRecoEnabled = true;
		TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
        var foundTransforms = GameObject.FindGameObjectsWithTag(Constants.MODEL3D_TAG);
        if (foundTransforms != null)
        {
            foreach (var found in foundTransforms)
                Destroy(found, 0);
        }
        Debug.Log("Cloud reco restarted");
	}

}