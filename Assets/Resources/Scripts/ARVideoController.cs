﻿using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using Vimeo.Player;

// Control Pipeline
// ARVideoController > VimeoPlayer > VideoController > VideoPlayer

[RequireComponent(typeof(VimeoPlayer))]
public class ARVideoController : MonoBehaviour
{
    #region PRIVATE_MEMBERS

    private VimeoPlayer vimeoPlayer;
    private Vector2 worldPositionAnchoredPosition;
    private Vector2 worldPositionAnchorMin;
    private Vector2 worldPositionAnchorMax;
    private Vector2 worldPositionSizeDelta;
    private Quaternion worldPositionRotation;
    private Vector3 worldPositionScale;
    private Vector2 canvasSizeDelta = new Vector2(0f, 0f);
    private Transform originalParent;

    #endregion //PRIVATE_MEMBERS


    #region PUBLIC_MEMBERS

    public Button m_PlayButton;
    public RectTransform elapsedBar;
    public RectTransform remainingBar;
    public RectTransform progressNub;
    public RawImage rawImage;
    public Canvas canvas;
    public RectTransform canvasRT;
    public RectTransform projectionPlane;
    public GameObject fullScreenBackground;
    public GameObject bufferingNotice;
    public bool isTracking = false;
    public Text progressTime;
    public Text reverseProgressTime;
    public Vector2 nubSize;
	public BrowserManager browserManager;
    public bool autolaunchWebsite = true;
    public ARUIHelper aRUIHelper;
    public CanvasGroup canvasGroup;
    public RectTransform container;
    public Vector2 horizontalMinAnchor;
    public Vector2 horizontalMaxAnchor;
    public Vector2 verticalMinAnchor;
    public Vector2 verticalMaxAnchor;

    #endregion //PRIVATE_MEMBERS


    #region MONOBEHAVIOUR_METHODS

    void Awake() {
        vimeoPlayer = GetComponent<VimeoPlayer>();
        nubSize = progressNub.anchorMax - progressNub.anchorMin;
    }

    void Start() {

        // Setup Delegates
        vimeoPlayer.OnLoadError += HandleVideoError;
        //vimeoPlayer.OnVideoStart += HandleStartedEvent;
        vimeoPlayer.OnPlay += HandleStartedEvent;
        vimeoPlayer.OnLoopPointReached += HandleLoopPointReached;

    }

    void Update() {

        if (bufferingNotice != null) {
            bufferingNotice.SetActive(vimeoPlayer.IsBuffering() && !vimeoPlayer.IsPlaying());
        }

        if (vimeoPlayer.IsPlaying()) {
            ShowPlayButton(false);

            // Update progress bar nub position
            SetSeekPosition(vimeoPlayer.GetProgress());

        } else {
            ShowPlayButton(true);
        }

        // Check for canvas aspect ratio modifications and fit the video into the canvas accordingly
        if (canvasSizeDelta != canvasRT.sizeDelta && !vimeoPlayer.loadingVideoMetadata && vimeoPlayer.GetWidth() != 0) { // Returns 0 before and while loading metadata
            float clipAspectRatio = (float)vimeoPlayer.GetWidth() / (float)vimeoPlayer.GetHeight();
            Debug.Log(vimeoPlayer.GetWidth() + " " + vimeoPlayer.GetHeight());
            float canvasAspectRatio = canvasRT.sizeDelta.x / canvasRT.sizeDelta.y;
            Debug.Log(canvasAspectRatio);
            // Following code should work for both landscape and portrait videos
            if (canvasAspectRatio >= clipAspectRatio) { // Canvas is wider than video
                float conversionDelta = (1f - (clipAspectRatio / canvasAspectRatio)) / 2f;
                projectionPlane.anchorMin = new Vector2(conversionDelta, 0f);
                projectionPlane.anchorMax = new Vector2(1f - conversionDelta, 1f);
            } else { // Video is wider than canvas
                float conversionDelta = (1f - (canvasAspectRatio / clipAspectRatio)) / 2f;
                projectionPlane.anchorMin = new Vector2(0f, conversionDelta);
                projectionPlane.anchorMax = new Vector2(1f, 1f - conversionDelta);
            }
            canvasSizeDelta = canvasRT.sizeDelta;
        }
        
    }

    // Handles progress nub positioning, also scaling elapsed and remaining colored time bars
    public void SetSeekPosition (float percentage, bool setTime = false) {
        
        if (elapsedBar != null && remainingBar != null && progressNub != null) {
            progressNub.anchorMin = new Vector2(percentage, 0.5f) - nubSize / 2f;
            progressNub.anchorMax = new Vector2(percentage, 0.5f) + nubSize / 2f;
            elapsedBar.anchorMax = new Vector2(percentage, 1f);
            remainingBar.anchorMin = new Vector2(percentage, 0f);
            System.TimeSpan timeSpan = System.TimeSpan.FromSeconds(Mathf.Floor(vimeoPlayer.GetCurrentTime()));
            aRUIHelper.ProcessRemainingVideoTime(Mathf.CeilToInt(vimeoPlayer.GetTotalTime() - (float)timeSpan.TotalSeconds)); // Update countdown
            if (progressTime != null && reverseProgressTime != null) { // Update time display too
                progressTime.text = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
                timeSpan = System.TimeSpan.FromSeconds(vimeoPlayer.GetTotalTime() - Mathf.Floor(vimeoPlayer.GetCurrentTime()));
                reverseProgressTime.text = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
            }
            if (setTime) {
                vimeoPlayer.Seek(percentage);
            }
        }
        
    }

    void OnApplicationPause(bool pause) {
        Debug.Log("OnApplicationPause(" + pause + ") called.");
        if (pause)
            Pause();
    }

    #endregion // MONOBEHAVIOUR_METHODS


    #region PUBLIC_METHODS

    public void Play() {
        Debug.Log("Play Video");
        vimeoPlayer.Play();
        ShowPlayButton(false);
    }

    public void Pause() {
        if (vimeoPlayer) {
            Debug.Log("Pause Video");
            //PauseAudio(true);
            vimeoPlayer.Pause();
            ShowPlayButton(true);
        }
    }

    public void Stop() {
        if (vimeoPlayer) {
            Debug.Log("Stop Video");
            vimeoPlayer.Stop();
            aRUIHelper.ProcessRemainingVideoTime(0);
            ShowPlayButton(true);
        }
    }

    // Store world position and switch canvas to Screen Space, or reverse
    public void ToggleFullScreen() {
        if (canvas.renderMode == RenderMode.WorldSpace) {
            worldPositionAnchoredPosition = canvasRT.anchoredPosition;
            worldPositionAnchorMin = canvasRT.anchorMin;
            worldPositionAnchorMax = canvasRT.anchorMax;
            worldPositionSizeDelta = canvasRT.sizeDelta;
            worldPositionRotation = canvasRT.rotation;
            worldPositionScale = canvasRT.localScale;
            fullScreenBackground.SetActive(true); // Show black background when switching to full screen
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            originalParent = transform.parent; // Store this object's parent
            transform.SetParent(null); // Detach from parent so that full screen video won't disappear when we lose tracking
        } else {
            canvas.renderMode = RenderMode.WorldSpace;
            fullScreenBackground.SetActive(false);
            canvasRT.anchoredPosition = worldPositionAnchoredPosition;
            canvasRT.anchorMin = worldPositionAnchorMin;
            canvasRT.anchorMax = worldPositionAnchorMax;
            canvasRT.sizeDelta = worldPositionSizeDelta;
            canvasRT.rotation = worldPositionRotation;
            canvasRT.localScale = worldPositionScale;
            originalParent = transform.parent; // Store this object's parent
            if (isTracking) {
                transform.SetParent(originalParent);
            } else if (!isTracking){
                Pause();
                gameObject.SetActive(false); // Disable if tracking was lost
            }
        }
    }

    public void ToggleFixedProjection () {
        if (canvas.renderMode == RenderMode.WorldSpace) {
            if (!browserManager.isBrowserOpen && !aRUIHelper.loadingNewScene) {
                browserManager.deviceOrientationManager.SetScreenOrientation(ScreenOrientation.Landscape);
            }
            worldPositionAnchoredPosition = canvasRT.anchoredPosition;
            worldPositionAnchorMin = canvasRT.anchorMin;
            worldPositionAnchorMax = canvasRT.anchorMax;
            worldPositionSizeDelta = canvasRT.sizeDelta;
            worldPositionRotation = canvasRT.rotation;
            worldPositionScale = canvasRT.localScale;
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            container.GetComponent<Image>().color = new Color(0f, 0f, 0f, 1f);
            originalParent = container.transform.parent; // Store this object's parent
            transform.SetParent(null); // Detach from parent so that full screen video won't disappear when we lose tracking
            UpdateFixedProjectionPosition();
        } else {
            browserManager.deviceOrientationManager.SetScreenOrientation(ScreenOrientation.AutoRotation);
            canvas.renderMode = RenderMode.WorldSpace;
            container.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
            canvasRT.anchoredPosition = worldPositionAnchoredPosition;
            canvasRT.anchorMin = worldPositionAnchorMin;
            canvasRT.anchorMax = worldPositionAnchorMax;
            canvasRT.sizeDelta = worldPositionSizeDelta;
            canvasRT.rotation = worldPositionRotation;
            canvasRT.localScale = worldPositionScale;
            container.anchorMin = Vector2.zero;
            container.anchorMax = Vector2.one;
            transform.SetParent(originalParent);
        }

    }

    public void UpdateFixedProjectionPosition () {
        if (canvas.renderMode == RenderMode.ScreenSpaceOverlay) {
            if (Screen.orientation == ScreenOrientation.Portrait) {
                container.anchorMin = verticalMinAnchor;
                container.anchorMax = verticalMaxAnchor;
            } else {
                container.anchorMin = horizontalMinAnchor;
                container.anchorMax = horizontalMaxAnchor;
            }
        }
    }

    public void HidePlayer () {
        canvasGroup.alpha = 0f;
    }

    public void ShowPlayer () {
        canvasGroup.alpha = 1f;
    }

    #endregion // PUBLIC_METHODS


    #region PRIVATE_METHODS

    private void ShowPlayButton(bool enable) {
        m_PlayButton.enabled = enable;
        m_PlayButton.GetComponent<Image>().enabled = enable;
    }

    public bool IsPlaying () {
        return vimeoPlayer.IsPlaying();
    }
    
    public void SetClip (string newClip) {
        Debug.Log(newClip.ToString());
        vimeoPlayer.PlayVideo(newClip);
    }

    public void UnloadVideo () {
        vimeoPlayer.vimeoVideo = null;
    }

    public void DisableWebsiteAutolaunch () {
        autolaunchWebsite = false;
    }
    
    #endregion // PRIVATE_METHODS


    #region DELEGATES

    void HandleVideoError() {
        Debug.LogError("Error: Vimeo could not handle the video");
    }

    void HandleStartedEvent() {
        Debug.Log("Started: Vimeo video");
        aRUIHelper.ToggleAutolaunchButtons(true);
        aRUIHelper.ToggleWebsiteButtons(true);
        autolaunchWebsite = true;
    }

    void HandleLoopPointReached() {
        Debug.Log("Vimeo video reached the loop point");
        if (autolaunchWebsite) {
            browserManager.OpenInAppBrowser();
            Stop();
            canvasGroup.alpha = 0f;
            aRUIHelper.ToggleWebsiteButtons(false);
        }
    }

    #endregion //DELEGATES

}
