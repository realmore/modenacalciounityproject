﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class DraggableNubSeek : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public ARVideoController videoController;

    private bool wasPlaying = false;

    public void OnBeginDrag(PointerEventData eventData) {
        wasPlaying = videoController.IsPlaying();
        if (wasPlaying) {
            videoController.Pause();
        }
    }

    public void OnDrag(PointerEventData data) {

        // Convert touched point to normalized coordinates of the progress bar's rect transform
        RectTransform seekBarRT = transform.parent.GetComponent<RectTransform>();
        Vector2 lp;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(seekBarRT, data.position, null, out lp);
        float xPosition = lp.x / seekBarRT.rect.width;
        if (xPosition < 0) {
            xPosition = 0;
        } else if (xPosition > 1) {
            xPosition = 1;
        }
        videoController.SetSeekPosition(xPosition, true);

    }

    public void OnEndDrag(PointerEventData eventData) {
        if (wasPlaying) {
            videoController.Play();
        }
    }

}