﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneSignalHandler : MonoBehaviour {
    void Start () {
        // Enable line below to enable logging if you are having issues setting up OneSignal. (logLevel, visualLogLevel)
        // OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.INFO, OneSignal.LOG_LEVEL.INFO);
    
        OneSignal.StartInit("906c617f-7f6e-40b1-8e2a-19bf1ee7bd66")
            .HandleNotificationOpened(HandleNotificationOpened)
            .EndInit();
    
        OneSignal.inFocusDisplayType = OneSignal.OSInFocusDisplayOption.Notification;
    }

    // Gets called when the player opens the notification.
    private static void HandleNotificationOpened(OSNotificationOpenedResult result) {
    }
}
