﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFade : MonoBehaviour
{
    public float fadeOutTime;
    public GameObject textObj;
    public GameObject panelObj;

    public void FadeOut()
    {
        StartCoroutine(FadeOutRoutine());
    }

    private IEnumerator FadeOutRoutine()
    {
        Text txt = textObj.GetComponent<Text>();
        Image img = panelObj.GetComponent<Image>();

        Color txtColor = txt.color;
        Color imgColor = img.color;
        for(float t = 0; t <= 1; t += Time.deltaTime)
        {
            img.color = new Color(imgColor.r, imgColor.g, imgColor.g, t);
            txt.color = new Color(txtColor.r, txtColor.g, txtColor.g, t);
            yield return null;
        }

    }

    public void FadeIn()
    {
        StartCoroutine(FadeInRoutine());
    }

    private IEnumerator FadeInRoutine()
    {
        Text txt = textObj.GetComponent<Text>();
        Image img = panelObj.GetComponent<Image>();

        Color txtColor = txt.color;
        Color imgColor = img.color;
        for (float t = fadeOutTime; t >= 0; t -= Time.deltaTime)
        {
            img.color = new Color(imgColor.r, imgColor.g, imgColor.g, t);
            txt.color = new Color(txtColor.r, txtColor.g, txtColor.g, t);
            yield return null;
        }
    }
}

