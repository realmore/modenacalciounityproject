using System.Collections;
using UnityEngine;

public class AutoFocusHandler : MonoBehaviour {

	bool switchingFocus = false;

	void OnEnable() {
		if (Vuforia.CameraDevice.Instance == null) {
			return;
		}
		//if (!switchingFocus) {
			StartCoroutine (switchfocus ());
		//}
	}
	
	void Update () {
		#if UNITY_EDITOR
		if (Input.GetMouseButtonUp (0) && !switchingFocus) {
			if (true) {
		#else
		if (Input.touchCount > 0 || Input.GetMouseButtonUp (0) && !switchingFocus) {
			if (Input.GetTouch (0).phase == TouchPhase.Ended && Input.GetTouch (0).deltaPosition.sqrMagnitude < 0.5f) {
		#endif
				StartCoroutine (switchfocus ());
			}
		}
	}

	IEnumerator switchfocus() {
		switchingFocus = true;
		Vuforia.CameraDevice.Instance.SetFocusMode(Vuforia.CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
		yield return new WaitForSeconds (0.2f);
		Vuforia.CameraDevice.Instance.SetFocusMode(Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		switchingFocus = false;
	}

	void OnDisable() {
		StopAllCoroutines();
		switchingFocus = false;
	}
}