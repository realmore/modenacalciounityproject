﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DigitsNFCToolkit;

public class MainMenuUI : MonoBehaviour {

    public void Start() {
#if (!UNITY_EDITOR)
		NativeNFCManager.AddNFCTagDetectedListener(OnNFCTagDetected);
		NativeNFCManager.AddNDEFReadFinishedListener(OnNDEFReadFinished);
		Debug.Log("NFC Tag Info Read Supported: " + NativeNFCManager.IsNFCTagInfoReadSupported());
		Debug.Log("NDEF Read Supported: " + NativeNFCManager.IsNDEFReadSupported());
		Debug.Log("NDEF Write Supported: " + NativeNFCManager.IsNDEFWriteSupported());
		Debug.Log("NFC Enabled: " + NativeNFCManager.IsNFCEnabled());
		Debug.Log("NDEF Push Enabled: " + NativeNFCManager.IsNDEFPushEnabled());
#endif
	}

	private void OnEnable() {
#if (!UNITY_EDITOR) && !UNITY_IOS
			NativeNFCManager.Enable();
#endif
	}

	private void OnDisable() {
#if (!UNITY_EDITOR) && !UNITY_IOS
			NativeNFCManager.Disable();
#endif
	}

	public void OnStartNFCReadClick() {
#if (!UNITY_EDITOR)
		NativeNFCManager.ResetOnTimeout = true;
		NativeNFCManager.Enable();
#endif
	}

	public void OnNFCTagDetected(NFCTag tag) { }

	public void OnNDEFReadFinished(NDEFReadResult result)
	{
		string readResultString = string.Empty;
		if(result.Success)
		{
			readResultString = string.Format("NDEF Message was read successfully from tag {0}", result.TagID);
		}
		else
		{
			readResultString = string.Format("Failed to read NDEF Message from tag {0}\nError: {1}", result.TagID, result.Error);
		}
		Debug.Log(readResultString);
	}

    public void OpenARScene () {
        StartCoroutine(OpenSceneAsync("AR"));
    }

    public void OpenNFCScene()
    {
        StartCoroutine(OpenSceneAsync("NFC"));
    }

    public void OpenMenuScene()
    {
        StartCoroutine(OpenSceneAsync("Main"));
    }

    IEnumerator OpenSceneAsync (string scene) {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene);
        while (!asyncLoad.isDone) {
            yield return null;
        }
        
    }

    public void OpenGroundPlaneScene()
    {
        StartCoroutine(OpenGroundPlaneSceneAsync("GroundPlane"));
    }

    IEnumerator OpenGroundPlaneSceneAsync(string scene)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    public void CloseApplication()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}
