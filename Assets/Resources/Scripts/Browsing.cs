﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Browsing : MonoBehaviour
{
    public BrowserManager browser;
    public GameObject WritingButton;
    public bool counter = false;
    public string username;

    // Start is called before the first frame update
    void Start()
    {
        browser.uvw.SetShowSpinnerWhileLoading(true);
        browser.OpenInAppBrowser();        
    }
    
    void Update() {
        if(browser.uvw.Url.Contains("myvizual.com/myvizual.php")) {
            browser.uvw.EvaluateJavaScript("name;", (payload) => {
                username = payload.data;
            });
        }

        if(username != "" && username != null){
            WritingButton.SetActive(true);
        }else {
            WritingButton.SetActive(false);
        }
        
    }

    public string getUsername(){
        return username;
    }


    /*
    void Update() {
        


        if(browser.uvw.Url.Contains("myvizual.com/myvizual.php")) {
            WritingButton.SetActive(true);

            browser.uvw.EvaluateJavaScript("name;", (payload) => {
                username = payload.data;
                if(payload.data != "" && payload.data != null) {
                    print("Username: " + payload.data);
                }
            });
        } else {
            WritingButton.SetActive(false);
        }
    }
    */
}
