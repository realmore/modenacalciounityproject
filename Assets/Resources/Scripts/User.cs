﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class User : MonoBehaviour {

    public string[] field = new string[15];
    public int selected_index = 15;
    public string selected_social;

    public InputField[] txts = new InputField[15];

    public Button[] buttons = new Button[15];
    public Sprite[] toggles_images = new Sprite[2];

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    /// 

    public void SaveUser() {
        for (int i = 0; i < field.Length; i++) {
            field[i] = txts[i].text;
        }

        SaveSystem.SaveUserProfile(this);
    }

    public void SetButton(int index) {
        for (int i = 0; i < buttons.Length; i++) {
            if (i == index) {
                buttons[i].image.sprite = toggles_images[1];
            } else {
                buttons[i].image.sprite = toggles_images[0];
            }
        }
        selected_index = index;
        selected_social = DefineSocial();
    }

    public string DefineSocial() {
        switch(selected_index){
            case 0:
                return "https://www.facebook.com/";
            break;
            case 1:
                return "https://www.instagram.com/";
            break;
            case 2:
                return "https://www.twitter.com/";
            break;
            case 3:
                return "https://www.linkedin.com/";
            break;
            case 4:
                return "https://wa.me/";
            break;
            case 5:
                return "tel:";
            break;
            case 6:
                return "mailto:";
            break;
            case 7:
                return "https://";
            break;
            case 8:
                return "skype:";
            break;
            case 9:
                return "paypal.me/";
            break;
            case 10:
                return "https://www.youtube.com/user/";
            break;
            case 11:
                return "https://www.snapchat.com/add/";
            break;
            case 12:
                return "https://www.tiktok.com/";
            break;
            case 13:
                return "viber://contact?number=%2B";
            break;
            case 14:
                return "https://www.vimeo.com/";
            break;
        }
        return "";
    }

    public void LoadUser() {
        UserProfile data = SaveSystem.LoadPlayer();
        selected_index = data.selected_index;
        selected_social = data.selected_social;
        for (int i = 0; i < field.Length; i++) {
            field[i] = data.field[i];
            if (field[i] != null || field[i] != "") {
                txts[i].text = field[i];
            }
            if (i == selected_index) {
                buttons[i].image.sprite = toggles_images[1];
            } else {
                buttons[i].image.sprite = toggles_images[0];
            }
        }
    }


}
