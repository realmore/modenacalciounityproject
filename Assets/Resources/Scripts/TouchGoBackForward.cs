﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchGoBackForward : MonoBehaviour
{
    BrowserManager browser;
    bool isSwipe = false;
    bool isTouch = false;
    Vector3 startPos;
    float minSwipeDistX, minSwipeDistY;
    bool isJump = false;

    // Start is called before the first frame update
    void Start()
    {
        browser = GameObject.Find("Browser").GetComponent<BrowserManager>();
        minSwipeDistX = minSwipeDistY = Screen.width / 6;
    }

    // Update is called once per frame
    void Update()
    {
        if (!browser.isBrowserOpen)
            return;

#if UNITY_ANDROID || UNITY_IPHONE
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    startPos = touch.position;
                    break;
                case TouchPhase.Moved:
                    isSwipe = true;
                    float swipeDistHorizontal = (new Vector3(touch.position.x, 0, 0) - new Vector3(startPos.x, 0, 0)).magnitude;
                    float swipeDistVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;
                    if (swipeDistHorizontal > minSwipeDistX)
                    {
                        float swipeValue = Mathf.Sign(touch.position.x - startPos.x);
                        if (swipeValue > 0 && !isTouch)//to right swipe
                        {
                            Debug.Log("Swipe to right");
                            if (browser.uvw.CanGoBack)
                                browser.uvw.GoBack();
                            isTouch = true;

                        }
                        else if (swipeValue < 0 && !isTouch)//to left swipe
                        {
                            Debug.Log("Swipe to Left");
                            if (browser.uvw.CanGoForward)
                                browser.uvw.GoForward();
                            isTouch = true;
                        }
                    }

                    break;
                case TouchPhase.Ended:
                    isSwipe = false;
                    isTouch = false;
                    break;
            }
        }
#endif
    }

}
