﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationGif : MonoBehaviour
{
    public Sprite[] animatedGifs;
    public Sprite checkedImg;
    public Sprite errorImage;
    public Image imageObj;

    public bool reading;
    public bool error = false;

    void Start(){
        imageObj.preserveAspect = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(reading && error == false) {
            imageObj.color = Color.white;
            imageObj.sprite = animatedGifs[(int)(Time.time*10)%animatedGifs.Length];
        } else if (reading == false && error == false) {
            imageObj.color = Color.green;
            imageObj.sprite = checkedImg;
        } else {
            imageObj.color = Color.white;
            imageObj.sprite = errorImage;
        }
        
    }
}
