﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebViewSizeRefresher : UnityEngine.EventSystems.UIBehaviour
{

	public UniWebView uvw;

    protected override void Start() {
        base.Start();
        uvw.UpdateFrame();
    }

	protected override void OnRectTransformDimensionsChange() {
		base.OnRectTransformDimensionsChange();
		// This will update web view's frame to match the reference rect transform if set.
		uvw.UpdateFrame();
	}

}
