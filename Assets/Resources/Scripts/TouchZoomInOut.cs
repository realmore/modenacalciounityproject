﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchZoomInOut : MonoBehaviour
{
    public float zoomSpeed = 0.01f;
  
    private GameObject model;
    private Vector3 minScale = new Vector3(1,1,1);
    private Vector3 maxScale = new Vector3(1, 1, 1);

    void Start()
    {
        model = GameObject.FindGameObjectWithTag(Constants.MODEL3D_TAG);
        minScale = model.transform.localScale;
        maxScale += model.transform.localScale * 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (model == null)
            return;

        // If there are two touches on the device...
        if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            float moltiplicator = deltaMagnitudeDiff * zoomSpeed;

            model.transform.localScale -= Vector3.one * moltiplicator;
            model.transform.localScale = Vector3.Max(model.transform.localScale, minScale);
            model.transform.localScale = Vector3.Min(model.transform.localScale, maxScale);
        }
        else
        {
            foreach(Touch t in Input.touches)
            {
                switch(t.phase)
                {
                    case TouchPhase.Stationary:
                        Ray ray = Camera.main.ScreenPointToRay(t.position);
                        RaycastHit hit;
                        if (Physics.Raycast(ray, out hit))
                        {
                            Vector3 v = model.transform.rotation.eulerAngles;
                            model.transform.rotation = Quaternion.Euler(v.x, v.y + 1, v.z);
                        }
                        break;
                }
            }
        }
    }
}
