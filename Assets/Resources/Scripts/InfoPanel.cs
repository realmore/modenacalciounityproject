﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoPanel : MonoBehaviour
{

    public string sampleMaterialURI;

    private CanvasGroup cg;

    void Awake () {
        cg = GetComponent<CanvasGroup>();
    }

    void Start ()
    {
        ToggleInfoPanel(false);
    }

    public void ToggleInfoPanel (bool active) {
        cg.alpha = active ? 1f : 0f;
        cg.interactable = active;
        cg.blocksRaycasts = active;
    }

    public void OpenDownloadURI () {
        Application.OpenURL(sampleMaterialURI);
    }

}
