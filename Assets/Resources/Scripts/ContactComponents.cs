﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContactComponents : MonoBehaviour
{
    public string name;
    public Text name_text;

    public string phone;
    public Text phone_txt;

    public string mail;
    public Text mail_txt;

    public string website;
    public Text website_txt;

    void Start() {
        name_text.text = name;
        phone_txt.text = phone;
        mail_txt.text = mail;
        website_txt.text = website;
    }
}
