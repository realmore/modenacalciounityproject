﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceOrientation : MonoBehaviour {
    
    public ScreenOrientation orientationAtSceneStart = ScreenOrientation.AutoRotation;

    void Start() {
        SetScreenOrientation(orientationAtSceneStart);
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = false;
    }
    
    public void SetScreenOrientation (ScreenOrientation screenOrientation) {
        Screen.orientation = screenOrientation;
    }

}
