﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILayoutOrientationSwitch : MonoBehaviour
{

    public CanvasGroup verticalLayout;
    public CanvasGroup horizontalLayout;
    public ARVideoController aRVideoController;

    private ScreenOrientation lastOrientation = ScreenOrientation.AutoRotation;

    void Update()
    {
        if (Screen.orientation != lastOrientation) {
            verticalLayout.interactable = Screen.orientation == ScreenOrientation.Portrait;
            horizontalLayout.interactable = !verticalLayout.interactable;
            verticalLayout.blocksRaycasts = Screen.orientation == ScreenOrientation.Portrait;
            horizontalLayout.blocksRaycasts = !verticalLayout.blocksRaycasts;
            verticalLayout.alpha = Screen.orientation == ScreenOrientation.Portrait ? 1f : 0f;
            horizontalLayout.alpha = Screen.orientation == ScreenOrientation.Portrait ? 0f : 1f;
            if (aRVideoController != null)
            {
                aRVideoController.UpdateFixedProjectionPosition();
            }
        }
    }
}
